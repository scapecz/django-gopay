__author__ = 'snakey'

from gopay.api.GopayHelper import PaymentStatus, PaymentConstants
from gopay import settings as gopay_settings
from gopay.models import Order
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext
from django.contrib import messages


def notification_callback(request, status, payment_session_id):
    order = Order.objects.get(payment_session_id=payment_session_id)
    if PaymentStatus.STATE_PAID == status.sessionState:
        #overeni stavu objednavky v ramci eshop
        #a) objednavka neni uhrazena => uhrada objednavky
        #b) objednavka je uhrazena => OK - znovu jiz nehradime - notifikace je dorucovana,
        #dokud E-shop nevrati HTTP result code 200
        #c) objednavka v jinem stavu => zalogovani situace - informovani spravce systemu
        #HTTP result code 200 - notifikaci dorucujeme, dokud neni prijata
        pass
    elif PaymentStatus.STATE_CANCELED == status.sessionState:
        #kontrola stavu objednavky v E-shopu
        #a) objednavku je mozne zrusit - zruseni objednavky
        #b) objednavku neni mozne zrusit - zalogovani situace - informovani spravce
        #HTTP result code 200 - notifikaci dorucujeme, dokud neni prijata
        pass

    elif PaymentStatus.STATE_TIMEOUTED == status.sessionState:
        #'timeouted'
        pass

    elif PaymentStatus.STATE_REFUNDED == status.sessionState:
        #storno platby na zaklade pozadavku obchodnika, nebo zakaznika
        #HTTP result code 200 - notifikaci dorucujeme, dokud neni prijata
        pass
    else:
        #zalogovani situace - informovani spravce
        #HTTP result code 200 - notifikaci dorucujeme, dokud neni prijata
        pass
        #endif
    return HttpResponse(status=200)


def process_callback(request, status, payment_session_id):
    constants = PaymentConstants()
    order = Order.objects.get(payment_session_id=payment_session_id)
    request.session['gopay_order_id'] = order.id
    #uspesne pripady
    if PaymentStatus.STATE_PAID == status.sessionState or \
                    PaymentStatus.STATE_PAYMENT_METHOD_CHOSEN == status.sessionState:
        order.state = status.sessionState
        order.save()
        message = constants.msgReturnUrl(status.sessionState)
        if order.state == PaymentStatus.STATE_PAYMENT_METHOD_CHOSEN:
            addMessage = constants.addMessage(status.sessionSubState)
            messages.info(request, addMessage)
        messages.success(request, message)
        return redirect(gopay_settings.GOPAY_SUCCESS_URL)

        #zrusena platba
    elif PaymentStatus.STATE_CANCELED == status.sessionState:
        #zakaznik provedl zruseni platby
        order.state = PaymentStatus.STATE_CANCELED
        order.save()
        message = constants.msgReturnUrl(status.sessionState)
        messages.error(request, message)
        return redirect(gopay_settings.GOPAY_FAIL_URL)

        #neplatny stav
    else:
        message = "Illegal state"
        messages.error(request, message)
        return redirect(gopay_settings.GOPAY_FAIL_URL)



# -*- coding: utf-8 -*-
__author__ = 'snakey'

from django.conf import settings
from django.core.urlresolvers import reverse
from gopay.api.GopayHelper import GopayHelper, PaymentStatus, GopayException, PaymentConstants

GOPAY_URL = getattr(settings, 'GOPAY_URL', 'https://testgw.gopay.cz/axis/EPaymentServiceV2')
GOPAY_REDIRECT = getattr(settings, 'GOPAY_REDIRECT', PaymentConstants.GOPAY_FULL_TEST)
GOPAY_ESHOP_GOID = getattr(settings, 'GOPAY_ESHOP_GOID', 8540279704l)
GOPAY_SECURE_KEY = getattr(settings, 'GOPAY_SECURE_KEY', "ocxgXEL5psb7PAllKuCSblc9")

GOPAY_FAIL_URL = getattr(settings, 'GOPAY_FAIL_URL', '/gopay/chyba')
GOPAY_SUCCESS_URL = getattr(settings, 'GOPAY_SUCCESS_URL', '/gopay/uspech')
GOPAY_CALLBACK_URL = getattr(settings, 'GOPAY_CALLBACK_URL', "http://localhost:8000/gopay/zpracovani")
GOPAY_NOTIFICATION_CALLBACK = getattr(settings, 'GOPAY_NOTIFICATION_CALLBACK', "gopay.utils.notification_callback")
GOPAY_PROCESS_CALLBACK = getattr(settings, 'GOPAY_PROCESS_CALLBACK', "gopay.utils.process_callback")

GOPAY_DEFAULT_PAYMENT_CHANNEL = getattr(settings, 'GOPAY_DEFAULT_PAYMENT_CHANNEL', None)
GOPAY_ALLOWED_PAYMENT_CHANNELS = getattr(settings, 'GOPAY_ALLOWED_PAYMENT_CHANNELS', None)




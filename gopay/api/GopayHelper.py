#!/usr/bin/env python2
# -*- coding: utf-8 -*-
'''
Created on Mar 7, 2012

@author: Bedrich Hovorka
'''

import SOAPpy
import re
from gopay.api.CryptoHelper import CryptoHelper
from datetime import date

CONCAT_DELIMITER = '|';
PRIMITIVE = (int, str, bool, long, dict, list)

class PaymentStatus:
    RESULT_CALL_COMPLETED = "CALL_COMPLETED"
    RESULT_CALL_FAILED = "CALL_FAILED"    
    
    STATE_CREATED = "CREATED"
    STATE_PAYMENT_METHOD_CHOSEN = "PAYMENT_METHOD_CHOSEN"
    STATE_AUTHORIZED = "AUTHORIZED"
    STATE_PAID = "PAID"
    STATE_CANCELED = "CANCELED"
    STATE_TIMEOUTED = "TIMEOUTED"
    STATE_REFUNDED = "REFUNDED"
    STATE_PARTIALLY_REFUNDED = "PARTIALLY_REFUNDED"
#endclass PaymentStatus


class PaymentResult:
    '''
     * Informacni element, ktery se pouziva pro informovani
     * o stavu zpracovaní pozadavku. 
    '''
    
    RES_ACCEPTED = "ACCEPTED"
    RES_FINISHED = "FINISHED"
    RES_FAILED = "FAILED"
#endclass PaymentResult

class PaymentConstants:

    # URL webove sluzby
    GOPAY_WS_ENDPOINT = "https://gate.gopay.cz/axis/EPaymentServiceV2"
    
    #URL testovaci webove sluzby
    GOPAY_WS_ENDPOINT_TEST = "http://testgw.gopay.cz/axis/EPaymentServiceV2"

    #URL testovaci platebni brany  -- uplna integrace
    GOPAY_FULL_TEST = "http://testgw.gopay.cz/gw/pay-full-v2"

    #URL platebni brany  -- uplna integrace 
    GOPAY_FULL = "https://gate.gopay.cz/gw/pay-full-v2"

    #URL vypisu plateb - testovaci brana
    GOPAY_ACCOUNT_STATEMENT = "https://testgw.gopay.cz/gw/services/get-account-statement"
   
    def __init__(self):
        # Popis stavu platby - messages
        self.RETURN_URL_MESSAGES = {}
        self.ADDITIONAL_MESSAGES = {}
        
        self.RETURN_URL_MESSAGES[PaymentStatus.STATE_CREATED] = u"Na platební bráně GoPay nebyla vybrána platební metoda. Pro dokončení platby pokračujte výběrem platební metody."
        self.RETURN_URL_MESSAGES[PaymentStatus.STATE_PAYMENT_METHOD_CHOSEN] = u"Platba byla úspěšně založena."
        self.RETURN_URL_MESSAGES[PaymentStatus.STATE_CANCELED] = u"Platba byla zrušena."
        self.RETURN_URL_MESSAGES[PaymentStatus.STATE_TIMEOUTED] = u"Platba byla zrušena."
        self.RETURN_URL_MESSAGES[PaymentStatus.STATE_PAID] = u"Platba byla úspěšně provedena."
        
        self.ADDITIONAL_MESSAGES["101"] = u"Čekáme na dokončení online platby. O jejím provedení Vás budeme neprodleně informovat."
        self.ADDITIONAL_MESSAGES["102"] = u"Instrukce pro dokončení platby Vám byly zaslány na emailovou adresu. O provedení platby Vás budeme budeme neprodleně informovat."
    
    def msgReturnUrl(self, key):
        return self.RETURN_URL_MESSAGES[key]
    
    def addMessage(self, key):
        return self.ADDITIONAL_MESSAGES[key]
#endclass PaymentContants

class GopayException(Exception):
    class Reason:
        OTHER=0
        INVALID_VS=1
        INVALID_GOID=2
        INVALID_SIGNATURE=3
        INVALID_CALL_STATE_STATE=4
        INVALID_PN=5
        INVALID_PRICE=6
        INVALID_STATUS_SIGNATURE=7
        INVALID_USERNAME=8
        INVALID_DESCRIPTION=9
        INVALID_CALL_RESULT=10
        
    def __init__(self, reason, message):
        '''
        Constructor
        '''
        Exception.__init__(self)
        self.reason = reason
        self.message = message
    #enddef __init__
    
    def __str__(self, *args, **kwargs):
        return "%s - d : %s %s" % (Exception.__str__(self), self.reason, self.message)
#endclass GopayException

#endclass 
class GopayHelper(object):
    '''
    * Pomocna trida pro platbu v systemu GoPay
    * 
    * - sestavovani retezcu pro podpis komunikacnich elementu
    * - sifrovani/desifrovani retezcu
    * - verifikace podpisu informacnich retezcu
    * 
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.cryptohelper = CryptoHelper()
        self.concatMethodPattern = re.compile(r'Types\.E?(.+)')
    #enddef __init__
    
    def __toStr(self, arg):            
        if self.isEmpty(arg):
            return ''
        elif isinstance(arg, bool):
            return '1' if arg else '0'
        elif isinstance(arg, date):
            return date.isoformat()
        elif isinstance(arg, SOAPpy.Types.booleanType):
            return '1' if arg._data else '0'
        elif isinstance(arg, SOAPpy.Types.longType):
            return str(arg._data)
        else:
            return str(arg)
    #enddef __toStr
    
    
    def isEmpty(self, obj):
        '''
         * Rozhoduje, zda je objekt "prazdny"
         * @param obj retezec
         * @return true je-li retezec prazdny (po odstraneni bilych znaku na zacatku i na konci) nebo null, jinak false
        '''
        if obj is None:
            return True
        if isinstance(obj, str):
            return len(obj.strip()) == 0
        if not isinstance(obj, PRIMITIVE):
            # kdyz vlastnost SOAPpy-objektu neni nastavena
            #print obj
            return obj.__dict__.has_key('_Method__call')
        return False
    #enddef isEmpty
           
    def concatArgs(self, *args):
        convertedArgs = map(self.__toStr, args)
        return CONCAT_DELIMITER.join(convertedArgs)
    #enddef concatArgs
    
    # v concatech je treba vyjmenovat kvuli kontrole ;)
    
    def concatPaymentCommand(self, paymentCommand, key):
        result = self.concatArgs(
                  paymentCommand.targetGoId, 
                  paymentCommand.productName, 
                  paymentCommand.totalPrice,
                  paymentCommand.currency, 
                  paymentCommand.orderNumber,
                  paymentCommand.failedURL, 
                  paymentCommand.successURL, 
                  paymentCommand.preAuthorization, 
                  paymentCommand.recurrentPayment, 
                  paymentCommand.recurrenceDateTo, 
                  paymentCommand.recurrenceCycle, 
                  paymentCommand.recurrencePeriod, 
                  paymentCommand.paymentChannels,
                  key);
        return result
    #enddef concatPaymentCommand
    
    
    def concatPaymentStatus(self, paymentStatus, key):
        '''
         * Sestaveni retezce pro podpis vysledku stav platby.
         * 
         * @param paymentStatus     vysledek overeni stavu platby
         * @param key                 heslo subjektu pro komunikaci s GoPay
         * @return retezec pro podpis
        '''
        return self.concatArgs(
            paymentStatus.targetGoId,
            paymentStatus.productName,
            paymentStatus.totalPrice,
            paymentStatus.currency,
            paymentStatus.orderNumber,
            paymentStatus.recurrentPayment,
            paymentStatus.parentPaymentSessionId,
            paymentStatus.preAuthorization,
            paymentStatus.result,
            paymentStatus.sessionState,
            paymentStatus.sessionSubState,
            paymentStatus.paymentChannel,
            key);
    #enddef concatPaymentStatus
    
    # FIXMY dalsi concat* kvuli kontrole
    
    def concatPaymentIdentity(self, paymentIdentity, key):
        '''
         * Sestaveni retezce pro podpis popisu platby (paymentIdentity).
         * 
         * @param paymentIdentity     popis platby 
         * @param key                 heslo subjektu pro komunikaci s GoPay
         * @return retezec pro podpis
        '''
        
        result = self.concatArgs(
            paymentIdentity.targetGoId,                
            paymentIdentity.paymentSessionId,
            paymentIdentity.parentPaymentSessionId,
            paymentIdentity.orderNumber,
            key);
        return result
    #enddef concatPaymentIdentity
    
    def concatPaymentChannels(self, *paymentChannels):
        '''
         * Vrati carkou oddeleny seznam platebnich metod
         * @param paymentChannels jednotlive plateni metody 
        '''
        if (paymentChannels is None or len(paymentChannels) < 1):
            return None;
        return ", ".join(paymentChannels)
    #enddef concatPaymentChannels
    
    def concatPaymentSessionInfo(self, paymentSession, key):
        '''
         * Sestaveni retezce pro podpis sessionInfo.
         * 
         * @param paymentSession     objekt obsahujici data pro dotazani se na stav platby 
         * @param key                 heslo subjektu pro komunikaci s GoPay
         * @return retezec pro podpis
        '''
        return self.concatArgs(paymentSession.targetGoId, paymentSession.paymentSessionId, key);
    #enddef concatPaymentSessionInfo
    
    def concatRecurrenceRequest(self, request, key):
        '''
         * Sestaveni retezce pro podpis sessionInfo.
         * 
         * @param request     objekt obsahujici data pro vytvoreni nasledne platby
         * @param key         heslo subjektu pro komunikaci s GoPay
         * @return retezec pro podpis
        '''
        return self.concatArgs(request.parentPaymentSessionId, request.targetGoId, request.orderNumber, request.totalPrice, key);
    #enddef concatRecurrenceRequest
    
    def concatRefundRequest(self, request, key):
        '''
         * Sestaveni retezce pro podpis sessionInfo.
         * 
         * @param request     objekt obsahujici data refundace
         * @param key         heslo subjektu pro komunikaci s GoPay
         * @return retezec pro podpis
        '''
        return self.concatArgs(request.targetGoId, request.paymentSessionId, request.amount, request.currency, request.description, key);
    #enddef concatRefundRequest
    
    def concatPaymentResult(self, result, key):
        '''
         * Sestaveni retezce pro podpis paymentResult.
         * 
         * @param request     objekt obsahujici data vysleduku operace
         * @param key         heslo subjektu pro komunikaci s GoPay
         * @return retezec pro podpis
        '''
        return self.concatArgs(result.paymentSessionId, result.result, key);
    #enddef
    
    def selectConcatMethod(self, obj):
        '''
           * Vyhleda pro SOAP objekt prislusnou concat metodu
           * - vyuzije infa ulozenem v SOAP objektu
           * - pomoci regularniho vyrazu prelozi info na nazev metody
           * - concat metody musi samozrejme dodrzovat prislusnou konvenci a ta uz zavedena je ;)
        '''
        if obj is None or isinstance(obj, PRIMITIVE):
            raise GopayException(GopayException.Reason.OTHER, "None or primitive object")

        if not obj.__dict__.has_key("_Method__name"):
            raise GopayException(GopayException.Reason.OTHER, "Non-SOAP object can't have concat-method")
        
        m = self.concatMethodPattern.sub('concat\\1', obj.__dict__["_Method__name"])
        try:
            return self.__getattribute__(m)
        except Exception, e:
            raise GopayException(GopayException.Reason.OTHER, "GopayHelper must have %s method"%m)
    #enddef selectConcatMethod
   
    def encrypt(self, data, key):
        '''
        * Sifrovani dat 3DES.
        *
        * @param data
        * @param key
        * @return sifrovany obsah v HEX forme
        '''
        return self.cryptohelper.encrypt(data, key);
    #enddef encrypt 
    
    def decrypt(self, data, key):
        '''
        * Desifrovani dat 3DES.
        *
        * @param data v HEX forme
        * @param key
        * @return desifrovany obsah
        '''
        return self.cryptohelper.decrypt(data, key);
    #enddef decrypt   
    
    def hash(self, data):
        '''
        * Vytvori hash ze zpravy.
        * 
        * @param data zprava
        * @return SHA hash zpravy, HEX reprezentace
        '''
        return self.cryptohelper.hash(data);
    #enddef hash
    
    def sign(self, obj, key):
        '''
         * Podepsani 
         * @param data     data k podepsani
         * @param key      heslo subjektu pro komunikaci s GoPay 
        '''
        concat = self.selectConcatMethod(obj)
        h = self.hash(concat(obj, key))
        obj.encryptedSignature = self.encrypt(h, key)
    #enddef sign
    
    def checkPaymentStatus(self, paymentStatus, goId, orderNumber, priceInCents, currency, productName, key):
        
        if not PaymentStatus.RESULT_CALL_COMPLETED == paymentStatus.result:
            raise GopayException(GopayException.Reason.INVALID_CALL_STATE_STATE, "PS invalid call state state [" + paymentStatus.resultDescription + "]")

        if not self.objEquals(orderNumber, paymentStatus.orderNumber):
            raise GopayException(GopayException.Reason.INVALID_VS, "PS invalid Order number orig[" + str(orderNumber) + "] current[" + str(paymentStatus.orderNumber) + "]")

        if not self.objEquals(productName, paymentStatus.productName):
            raise GopayException(GopayException.Reason.INVALID_PN, "PS invalid product name orig[" + productName + "] current[" + paymentStatus.getProductName() + "]")

        if not self.objEquals(goId, paymentStatus.targetGoId):
            raise GopayException(GopayException.Reason.INVALID_GOID, "PS invalid GoID orig[" + goId + "] current[" + paymentStatus.targetGoId + "]")

        if not self.objEquals(priceInCents, paymentStatus.totalPrice):
            raise GopayException(GopayException.Reason.INVALID_PRICE, "PS invalid price orig[" + priceInCents + "] current[" + paymentStatus.totalPrice + "]")
        
        if currency is not None and not self.objEquals(currency, paymentStatus.currency) :
            raise GopayException(GopayException.Reason.INVALID_PRICE, "PS invalid currency orig[" + currency + "] current[" + paymentStatus.getCurrency() + "]")
        
        hashedSignature = self.hash(self.concatPaymentStatus(paymentStatus, key))
        decryptedHash = self.decrypt(paymentStatus.encryptedSignature, key)

        if not self.objEquals(hashedSignature, decryptedHash):
            raise GopayException(GopayException.Reason.INVALID_STATUS_SIGNATURE, "PS invalid status signature")

        return True
    #enddef checkPaymentStatus
    
    def checkPaymentIdentity(self, paymentIdentity, goId, orderNumber, key):
        '''
         * Kontrola parametru predavanych ve zpetnem volani po potvrzeni/zruseni platby - verifikace podpisu.
         * 
         * @param paymentIdentity        vracena navratova hodnota z platby 
         * @param goId                    identifikace eshopu nebo uzivatele
         * @param orderNumber            orderNumber vracene v redirectu
         * @param key                     heslo subjektu pro komunikaci s GoPay
         * 
         * @return true kdyz je vse OK
        '''
        if not self.objEquals(paymentIdentity.orderNumber, orderNumber):
            raise GopayException(GopayException.Reason.INVALID_VS, "PI invalid order number")

        if not self.objEquals(paymentIdentity.targetGoId._data, goId):
            raise GopayException(GopayException.Reason.INVALID_GOID, "PI invalid GoID")

        hashedSignature = self.hash(self.concatPaymentIdentity(paymentIdentity, key));
        
        decryptedHash = self.decrypt(paymentIdentity.encryptedSignature, key);
        
        if not self.objEquals(hashedSignature, decryptedHash):
            raise GopayException(GopayException.Reason.INVALID_SIGNATURE,  "PI invalid signature")
        return True
    #enddef checkPaymentStatus
    
    def checkPaymentResult(self, callReturn, paymentSessionId, key):
        hashedSignature = self.hash(self.concatPaymentResult(callReturn, key));
        
        if isinstance(paymentSessionId, SOAPpy.Types.longType):
            paymentSessionId = paymentSessionId._data
        
        if not self.objEquals(callReturn.paymentSessionId, paymentSessionId):
            raise GopayException(GopayException.Reason.INVALID_VS, "PI invalid paymentSessionId")
        
        decryptedHash = self.decrypt(callReturn.encryptedSignature, key);
        
        if not self.objEquals(hashedSignature, decryptedHash):
            raise GopayException(GopayException.Reason.INVALID_SIGNATURE,  "PI invalid signature")
        return True
    #end checkPaymentResult

    def objEquals(self, str1, str2):
        '''
         * Rozhoduje, zda jsou dva retezce shodne
         * @param str1 prvni retezec
         * @param str2 druhy retezec
         * @return vraci true pokud jsou oba retezce nenullove a shodne, jinak false
        '''
        if str1 is None or str2 is None:
            return False
        result = str1 == str2
        return result
    #enddef 
        
#endclass GopayHelper
        
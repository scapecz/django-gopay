'''
Created on Mar 13, 2012

@author: Bedrich Hovorka

Rutiny pro overovani SSL certifikatu
'''
import httplib, ssl, socket, urllib2, base64
from SOAPpy import HTTPTransport, Config, SOAPAddress
from SOAPpy.Client import debugFooter, debugHeader, HTTPError, SOAPUserAgent, __version__

PATH_TO_SSL_CERTIFICATES = "/etc/ssl/certs/ca-certificates.crt"

# from http://thejosephturner.com/blog/2011/03/19/https-certificate-verification-in-python-with-urllib2/
class VerifiedHTTPSConnection(httplib.HTTPSConnection):
    def connect(self):
        # overrides the version in httplib so that we do
        #    certificate verification
        self.set_debuglevel(0)
        sock = socket.create_connection((self.host, self.port), self.timeout)
        if self._tunnel_host:
            self.sock = sock
            self._tunnel()
        # wrap the socket using verification with the root
        #    certs in trusted_root_certs
        self.sock = ssl.wrap_socket(sock,
                                    self.key_file,
                                    self.cert_file,
                                    cert_reqs=ssl.CERT_REQUIRED,
                                    ca_certs=PATH_TO_SSL_CERTIFICATES)
    #enddef connect
#endclass

# wraps https connections with ssl certificate verification
class VerifiedHTTPSHandler(urllib2.HTTPSHandler):
    def __init__(self, connection_class = VerifiedHTTPSConnection):
        self.specialized_conn_class = connection_class
        urllib2.HTTPSHandler.__init__(self)
    def https_open(self, req):
        return self.do_open(self.specialized_conn_class, req)
#endclass

# for SOAPpy
# modiefied HTTPTransport from SOAPpy
class VerifiedHTTPSTransport(HTTPTransport):
    def call(self, addr, data, namespace, soapaction = None, encoding = None,
        http_proxy = None, config = Config):

        if not isinstance(addr, SOAPAddress):
            addr = SOAPAddress(addr, config)

        # Build a request
        if http_proxy:
            real_addr = http_proxy
            real_path = addr.proto + "://" + addr.host + addr.path
        else:
            real_addr = addr.host
            real_path = addr.path

        headers = {}

        headers["Host"] = addr.host
        headers["User-agent"] = SOAPUserAgent()
        t = 'text/xml';
        if encoding != None:
            t += '; charset="%s"' % encoding
        headers["Content-type"] = t
        headers["Content-length"] = str(len(data))

        # if user is not a user:passwd format
        #    we'll receive a failure from the server. . .I guess (??)
        if addr.user != None:
            val = base64.encodestring(addr.user) 
            headers['Authorization'] = 'Basic ' + val.replace('\012','')

        # This fixes sending either "" or "None"
        if soapaction == None or len(soapaction) == 0:
            headers["SOAPAction"] = ""
        else:
            headers["SOAPAction"] = '"%s"' % soapaction

        if config.dumpHeadersOut:
            s = 'Outgoing HTTP headers'
            debugHeader(s)
            print "POST %s" % (real_path)
            print "Host:", addr.host
            print "User-agent: SOAPpy " + __version__ + " (http://pywebsvcs.sf.net)"
            print "Content-type:", t
            print "Content-length:", len(data)
            print 'SOAPAction: "%s"' % soapaction
            debugFooter(s)

        if config.dumpSOAPOut:
            s = 'Outgoing SOAP'
            debugHeader(s)
            print data,
            if data[-1] != '\n':
                print
            debugFooter(s)

        https_handler = VerifiedHTTPSHandler()
        opener = urllib2.build_opener(https_handler)
        request = urllib2.Request(addr.proto + "://" + addr.host + addr.path, data=data, headers=headers)
              
        response = opener.open(request)

        # read response line
        code, msg, headers = response.code, response.msg, response.headers

        if headers:
            content_type = headers.get("content-type","text/xml")
            content_length = headers.get("Content-length")
        else:
            content_type=None
            content_length=None

        # work around OC4J bug which does '<len>, <len>' for some reaason
        if content_length:
            comma=content_length.find(',')
            if comma>0:
                content_length = content_length[:comma]
        
        data = response.read()
        message_len = len(data)

        if(config.debug):
            print "code=",code
            print "msg=", msg
            print "headers=", headers
            print "content-type=", content_type
            print "data=", data
                
        if config.dumpHeadersIn:
            s = 'Incoming HTTP headers'
            debugHeader(s)
            if headers.headers:
                print "HTTP/1.? %d %s" % (code, msg)
                print "\n".join(map (lambda x: x.strip(), headers.headers))
            else:
                print "HTTP/0.9 %d %s" % (code, msg)
            debugFooter(s)

        def startswith(string, val):
            return string[0:len(val)] == val
        
        if code == 500 and not \
               ( startswith(content_type, "text/xml") and message_len > 0 ):
            raise HTTPError(code, msg)

        if config.dumpSOAPIn:
            s = 'Incoming SOAP'
            debugHeader(s)
            print data,
            if (len(data)>0) and (data[-1] != '\n'):
                print
            debugFooter(s)

        if code not in (200, 500):
            raise HTTPError(code, msg)


        # get the new namespace
        if namespace is None:
            new_ns = None
        else:
            new_ns = self.getNS(namespace, data)
        
        # return response payload
        return data, new_ns
    #enddef call
#endclass
        
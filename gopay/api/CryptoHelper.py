#!/usr/bin/env python2
# -*- coding: utf-8 -*-
'''
Created on Mar 7, 2012

@author: Bedrich Hovorka
'''
import hashlib
from M2Crypto.EVP import Cipher, EVPError

toHex = lambda x:"".join([hex(ord(c))[2:].zfill(2) for c in x])

def fromHex(hexStr):
    _bytes = []
    hexStr = ''.join( hexStr.split(" ") )
    for i in range(0, len(hexStr), 2):
        _bytes.append(chr(int(hexStr[i:i+2], 16)))
    return ''.join(_bytes)

# sifra se pouziva k symetrickemu podpisu (zasifrovani hashe - kratky plaintext)
CIPHER_ALGORITHM = 'des_ede3_ecb'
IV = '\0' * 16 # pro ECB mod se nepouzije

class CryptoHelper(object):
    '''
    Pomocna trida pro sifrovani
    '''

    def __init__(self):
        '''
        Constructor
        '''
        
    def hash(self, message):
        '''
        * Vytvori hash ze zpravy
        * @param message zprava
        * @return SHA hash zpravy, HEX reprezentace
        '''
        messageDigest = hashlib.sha1()
        messageDigest.update(message)
        return toHex(messageDigest.digest())
    #enddef hash
    
    def decrypt(self, encryptedHEX, key):
        '''
        * Desifrovani dat
        *
        * @param encryptedHEX
        * @param key
        * @return desifrovany retezec
        '''
        assert encryptedHEX is not None and key is not None;
        
        if len(key) < 24:
            raise Exception('encryption key must have lengt at least 24')
            
        des = Cipher(alg=CIPHER_ALGORITHM, key=key[:24], op=0, iv=IV)
        decrypted = des.update(fromHex(encryptedHEX))
        try:
            decrypted += des.final()
        except EVPError, e:
            # i kdyz se desifruje ok, hazi to vyjimku
            print 'WARNING from M2Crypto library:', e.message
        return decrypted.strip(chr(0))
    #enddef decrypt
    
    def encrypt(self, message, key):
        '''
        * Sifrovani dat 3DES
        *
        * @param message
        * @param key
        * @return sifrovany obsah v HEX forme
        '''
        if len(key) < 24:
            raise Exception('encryption key must have lengt at least 24')

        des = Cipher(alg=CIPHER_ALGORITHM, key=key[:24], op=1, iv=IV)
        fullfill = 8 - (len(message)%8)
        for i in xrange(fullfill):
            message += chr(0)
        encrypted = des.update(message)
        encrypted += des.final()
        return toHex(encrypted)
    #enddef encrypt
#endclass CryptoHelper
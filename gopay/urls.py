__author__ = 'snakey'

from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('gopay.views',

                       url(r'^zpracovani/$', 'process', name='gopay_process'),
                       url(r'^notification/$', 'notification', name='gopay_notification'),

                       #testovaci URL
                       #url(r'^platba/$', 'test_payment', name='gopay_test_payment'),
                       url(r'^uspech/$', 'success', name='gopay_success'),
                       url(r'^chyba/$', 'fail', name='gopay_fail'),

                       )

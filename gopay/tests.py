# -*- coding: utf-8 -*-
__author__ = 'snakey'
'''
Spoustej testy takto ./run test gopay --settings='settings_no_db'

'''


from django.utils import unittest

from gopay import convert_float_to_gopay_price, create_payment_command
from gopay.api.SSLHelper import VerifiedHTTPSTransport
from gopay import settings as gopay_settings
from gopay.api.CountryCode import CountryCode
from gopay.models import Order, OrderError
#import SOAPpy
from SOAPpy import SOAPProxy
#from gopay.api.GopayHelper import GopayHelper, PaymentStatus, GopayException, PaymentConstants

TEST_PAYMENT_DATA = {
    'productName': 'Testovaci produkt',
    'orderNumber': 'OV123456',
    'totalPrice': 10112l,
    'currency': 'CZK',

    'firstName': 'Josef',
    'lastName': 'Novak',
    'email': 'snakey@email.cz',
    'phoneNumber': '+420725698965',
    'postalCode': '37001',
    'street': 'Plana 67',
    'city': '37001',
    'countryCode': CountryCode.CZE

}

def create_test_order(only_required_attrs=False):
    order = Order()
    order.product_title = u'Testovací produkt 32 ščřšř 1'
    order.email = u'snakey@email.cz'
    order.price = 50
    if not only_required_attrs:
        order.first_name = u'Josef'
        order.last_name = u'Novák'
        #order.phone = u'+420725698965'
        order.postal_code = u'37001'
        order.street = u'Planá 67'
        order.city = u'České Budějovice'
        order.country_code = CountryCode.CZE
    order.save()
    return order


class HelpersTest(unittest.TestCase):

    def setUp(self):
        self.server = SOAPProxy(gopay_settings.GOPAY_URL, transport=VerifiedHTTPSTransport)

    def test_convert_float_to_gopay_price(self):
        res1 = convert_float_to_gopay_price(100)
        self.assertEqual(res1, 10000, "Chyba #1, vysl je %d" % res1)

        res2 = convert_float_to_gopay_price(100.1)
        self.assertEqual(res2, 10010, "Chyba #2, vysl je %d" % res2)

        res3 = convert_float_to_gopay_price(100.10)
        self.assertEqual(res3, 10010, "Chyba #3, vysl je %d" % res3)

        res4 = convert_float_to_gopay_price(100.1023)
        self.assertEqual(res4, 10010, "Chyba #4, vysl je %d" % res4)

    def test_create_payment_command(self):
        data = TEST_PAYMENT_DATA
        create_payment_command(self.server, data)

    def test_create_payment_command_from_order(self):
        order = create_test_order()
        order.create_payment_command(self.server)


class OrderTest(unittest.TestCase):
    def test_create_gopay_payment(self):
        order = create_test_order()
        order.create_payment_in_gopay()

    def test_get_gopay_gate_url(self):
        order = create_test_order()
        order.create_payment_in_gopay()
        url = order.get_redirect_url_to_gate()
        self.assertIsInstance(url, str, 'Chyba #5')

        order2 = create_test_order()
        e = None
        try:
            order2.get_redirect_url_to_gate()
        except OrderError as error:
            e = error

        self.assertIsInstance(e, OrderError, 'Chyba #6')













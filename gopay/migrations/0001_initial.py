# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Order'
        db.create_table('gopay_order', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('order_number', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('product_title', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('currency', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('payment_session_id', self.gf('django.db.models.fields.BigIntegerField')(db_index=True, null=True, blank=True)),
            ('pre_authorization', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('recurrent_payment', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('recurrence_date_to', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('recurrence_cycle', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('recurrence_period', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('postal_code', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('country_code', self.gf('django.db.models.fields.CharField')(max_length=5, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('p1', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('p2', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('p3', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('p4', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('state', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('gopay', ['Order'])


    def backwards(self, orm):
        # Deleting model 'Order'
        db.delete_table('gopay_order')


    models = {
        'gopay.order': {
            'Meta': {'object_name': 'Order'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'country_code': ('django.db.models.fields.CharField', [], {'max_length': '5', 'null': 'True', 'blank': 'True'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'order_number': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'p1': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'p2': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'p3': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'p4': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'payment_session_id': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'postal_code': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'pre_authorization': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'product_title': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'recurrence_cycle': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'recurrence_date_to': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'recurrence_period': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            'recurrent_payment': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'state': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['gopay']
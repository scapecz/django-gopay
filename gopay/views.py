# -*- coding: utf-8 -*-
__author__ = 'snakey'

import SOAPpy
from SOAPpy import SOAPProxy

from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from gopay import settings as gopay_settings, get_identity_from_request, read_payment_status
from gopay.api.SSLHelper import VerifiedHTTPSTransport
from gopay.api.GopayHelper import GopayHelper, PaymentConstants, PaymentStatus, GopayException
from gopay.tests import TEST_PAYMENT_DATA, create_test_order
from gopay.models import Order
from django.core.exceptions import ImproperlyConfigured

from django.utils.importlib import import_module


def test_payment(request):
    order = create_test_order(only_required_attrs=False)
    order.save()
    order.create_payment_in_gopay()
    url_to_redirect = order.get_redirect_url_to_gate()
    return HttpResponseRedirect(url_to_redirect)


def process(request):
    server = SOAPProxy(gopay_settings.GOPAY_URL, transport=VerifiedHTTPSTransport)

    callback_process_function = getattr(gopay_settings, 'GOPAY_PROCESS_CALLBACK')
    i = callback_process_function.rfind('.')
    module, attr = callback_process_function[:i], callback_process_function[i + 1:]
    try:
        callback_process_function = getattr(import_module(module), attr)
    except (ImportError, AttributeError), e:
        raise ImproperlyConfigured('GOPAY: Error loading callback_notification_function from module %s: "%s"' % (module, e))

    try:
        identity = get_identity_from_request(request)
        status = read_payment_status(server, identity)
        payment_session_id = identity.paymentSessionId._data
        return callback_process_function(request, status, payment_session_id)

    except GopayException:
        return redirect(gopay_settings.GOPAY_FAIL_URL)


def notification(request):
    server = SOAPProxy(gopay_settings.GOPAY_URL, transport=VerifiedHTTPSTransport)

    callback_notification_function = getattr(gopay_settings, 'GOPAY_NOTIFICATION_CALLBACK')
    i = callback_notification_function.rfind('.')
    module, attr = callback_notification_function[:i], callback_notification_function[i + 1:]
    try:
        callback_notification_function = getattr(import_module(module), attr)
    except (ImportError, AttributeError), e:
        raise ImproperlyConfigured('GOPAY: Error loading callback_notification_function from module %s: "%s"' % (module, e))

    try:
        identity = get_identity_from_request(request)
        status = read_payment_status(server, identity)
        payment_session_id = identity.paymentSessionId._data
        return callback_notification_function(request, status, payment_session_id)
    except GopayException:
        return redirect(gopay_settings.GOPAY_FAIL_URL)



# ukazka jak napriklad zpracovat uspesnou objednavku, dulezite vymazat session
def success(request):
    order_id = request.session.get('gopay_order_id')
    if order_id:
        del request.session['gopay_order_id']
        order = Order.objects.get(id=order_id)
    else:
        order = None
    return render_to_response('gopay/success.html',
                              {
                                  'order': order
                              },
                              context_instance=RequestContext(request))


def fail(request):
    return render_to_response('gopay/fail.html',
                              {

                              },
                              context_instance=RequestContext(request))

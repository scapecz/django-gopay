__author__ = 'snakey'
from django.contrib import admin
from gopay.models import Order


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'email', 'state', 'created_on')
    list_filter = ('state',)


admin.site.register(Order, OrderAdmin)
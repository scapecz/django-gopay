# -*- coding: utf-8 -*-
import SOAPpy
import string


from gopay import settings as gopay_settings
from gopay.api.CountryCode import CountryCode
from gopay.api.GopayHelper import GopayHelper, PaymentStatus, GopayException, PaymentConstants


class CreatePaymentError(Exception):
    pass


class Identity:
    targetGoId = None
    orderNumber = None
    paymentSessionId = None
    parentPaymentSessionId = None
    encryptedSignature = None


def convert_float_to_gopay_price(price_float):
    price = round(price_float, 2)
    price_str = str(price)
    price_pieces = price_str.split('.')

    if len(price_pieces) == 1:
        price_pieces[1] = '00'

    if len(price_pieces[1]) == 1:
        price_pieces[1] += '0'
    new_price_str = string.join(price_pieces, "")
    return long(new_price_str)


def create_payment_command(server, data, allowed_channels=gopay_settings.GOPAY_ALLOWED_PAYMENT_CHANNELS, default_channel=gopay_settings.GOPAY_DEFAULT_PAYMENT_CHANNEL):
    command = server.Types.EPaymentCommand

    command.failedURL = gopay_settings.GOPAY_CALLBACK_URL
    command.successURL = gopay_settings.GOPAY_CALLBACK_URL
    command.paymentChannels = allowed_channels
    command.defaultPaymentChannel = default_channel

    command.productName = data.get('productName')
    command.orderNumber = data.get('orderNumber')
    command.totalPrice = SOAPpy.longType(data.get('totalPrice'))
    command.currency = data.get('currency')
    command.targetGoId = SOAPpy.longType(gopay_settings.GOPAY_ESHOP_GOID)

    customerData = server.Types.ECustomerData
    customerData.firstName = data.get('firstName')
    customerData.lastName = data.get('lastName')
    customerData.email = data.get('email')
    customerData.phoneNumber = data.get('phoneNumber')
    customerData.postalCode = data.get('postalCode')
    customerData.street = data.get('street')
    customerData.city = data.get('city')
    customerData.countryCode = data.get('countryCode')
    command.customerData = customerData

    command.p1 = data.get('p1')
    command.p2 = data.get('p2')
    command.p3 = data.get('p3')
    command.p4 = data.get('p4')
    command.lang = data.get('lang')

    if not command.productName or not command.currency or not command.orderNumber or not customerData.email:
        raise CreatePaymentError('Some of required parameters isnt set.\
         Required parameteres are: productName, orderNumber, currency, email')

    gopayHelper = GopayHelper()
    gopayHelper.sign(command, gopay_settings.GOPAY_SECURE_KEY)
    return command


from gopay.models import Order


def read_payment_status(server, identity):
    #kontrola parametru z redirectu - podpis, vazba na spravnou objednavku
    try:
        order = Order.objects.get(payment_session_id=identity.paymentSessionId._data)
    except Order.DoesNotExist:
        raise GopayException(GopayException.Reason.OTHER, "Such Order doesnt exist in database")
    data = order.get_data_for_gopay()
    GopayHelper().checkPaymentIdentity(identity, gopay_settings.GOPAY_ESHOP_GOID, str(data['orderNumber']), gopay_settings.GOPAY_SECURE_KEY)
    info = server.Types.PaymentSessionInfo
    info.targetGoId = SOAPpy.longType(gopay_settings.GOPAY_ESHOP_GOID)
    info.paymentSessionId = identity.paymentSessionId
    GopayHelper().sign(info, gopay_settings.GOPAY_SECURE_KEY)
    status = server.paymentStatus(info)

    if not PaymentStatus.RESULT_CALL_COMPLETED == status.result:
        raise GopayException(GopayException.Reason.OTHER, "status failed [" + status.getResultDescription() + "] ")

    # kontrola parametru ve stavu platby

    GopayHelper().checkPaymentStatus(status, gopay_settings.GOPAY_ESHOP_GOID, str(data['orderNumber']), data['totalPrice'],
                                     str(data['currency']), str(data['productName']), gopay_settings.GOPAY_SECURE_KEY)

    return status


def get_identity_from_request(request):
    payment_session_id = request.GET.get('paymentSessionId')
    parent_payment_session_id = request.GET.get('parentPaymentSessionId')
    go_id = request.GET.get('targetGoId')
    order_number = request.GET.get('orderNumber')
    encrypted_signature = request.GET.get('encryptedSignature')
    if not payment_session_id or not go_id or not order_number or not encrypted_signature:
        raise GopayException(GopayException.Reason.OTHER, 'Identity from request doesnt have all required attributes')

    identity = Identity()
    identity.targetGoId = SOAPpy.longType(long(go_id))
    identity.orderNumber = str(order_number)
    identity.paymentSessionId = SOAPpy.longType(long(payment_session_id))

    if parent_payment_session_id:
        identity.parentPaymentSessionId = SOAPpy.longType(long(parent_payment_session_id))

    identity.encryptedSignature = str(encrypted_signature)
    return identity

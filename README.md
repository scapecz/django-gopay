Important
-------------
This application is not ready to use package but I believe it can still save you few hours of painful programming. 
It is used by me on three projects so I can guarantee it works.

Dependencies
-------------
* *SOAPpy==0.12.0* (Parts of code from original GoPay library is used and these parts are not compatible with newer SOAPpy)
* *M2Crypto* (Had an issue with version provided by pip, personally using http://github.com/tobiasherp/M2Crypto . Follow http://stackoverflow.com/questions/10547332/install-m2crypto-on-a-virtualenv-without-system-packages for more info about issue.)

Usage
-----------
```
# -*- coding: utf-8 -*-
from gopay.models import Order

order = Order.objects.create(product_title=u'test', price=300, currency='CZK', email='test@test.cz')
order.create_payment_in_gopay()
url_to_gopay_gate = order.get_redirect_url_to_gate()
#in view eg. return redirect(url_to_gopay_gate)
```